import { RedditService } from './service/reddit.service';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DetailsPage } from "../details/details";

@IonicPage()
@Component({
  selector: 'page-reddit',
  templateUrl: 'reddit.html',
})
export class RedditPage implements OnInit {
  items: any;
  category: string;
  limit: number;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private redditService: RedditService) {
      this.getDefaults();
  }

  ngOnInit() {
    this.getPosts(this.category, this.limit);
  }

  getDefaults() {
    var category = localStorage.getItem('category');
    if (category != null) {
      this.category = category;
    } else {
      this.category = 'sports';
    }

    var limit = localStorage.getItem('limit');
    if (limit != null) {
      this.limit = parseInt(limit);
    } else {
      this.limit = 10;
    }
  }

  getPosts(category, limit) {
    this.redditService.getPosts(category, limit).subscribe(res => {
      this.items = res.data.children;
    });
  }

  viewItem(item) {
    this.navCtrl.push(DetailsPage, {
      item: item
    });
  }

  changeCategory() {
    this.getPosts(this.category, this.limit);
  }
}
